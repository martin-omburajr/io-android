package com.example;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;
public class MainGenerator {
    private static final String PROJECT_DIR = System.getProperty("user.dir");
    public static void main(String[] args) {
        Schema schema = new Schema(1, "io.db");
        schema.enableKeepSectionsByDefault();
        addTables(schema);
        try {
            new DaoGenerator().generateAll(schema, PROJECT_DIR + "\\app\\src\\main\\java\\scla.io_android");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private static void addTables(final Schema schema) {
        Entity user = addUser(schema);
        Entity logos = addLogos(schema);
        //Property userId = repo.addLongProperty("userId").notNull().getProperty();
        //user.addToMany(repo, userId, "userRepos");
    }
    private static Entity addUser(final Schema schema) {
        Entity user = schema.addEntity("User");
        user.addStringProperty("id").primaryKey();
        user.addStringProperty("name").notNull();
        user.addShortProperty("age");
        return user;
    }
    private static Entity addLogos(final Schema schema) {
        Entity logos = schema.addEntity("Logos");
        logos.addStringProperty("id").primaryKey();
        logos.addStringProperty("logos").notNull();
        logos.addStringProperty("description").notNull();
        logos.addStringProperty("etymology");
        logos.addDateProperty("creationDate");
        return logos;
    }
}